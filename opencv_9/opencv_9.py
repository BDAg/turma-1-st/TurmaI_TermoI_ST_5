# encoding: utf-8
  #!/usr/bin/python
  #Python 2/3 compatibility
#! / Usr / local / bin / python
from __future__ import print_function
from matplotlib import pyplot as plt
import cv2
import numpy as np
from Tkinter import *



def filter_mask (im_filter_mask):


	kernel = np.ones((1,1), np.uint8) #VARIAVEL QUE DEFINE O TAMANHO DA MATRIZ PARA APLICAR O FILTRO
	erode = cv2.erode(im_filter_mask,kernel,iterations = 1) #APLICA O FILTRO DE EROSAO 2 VEZES
						#IMAGEM     ,MATRIZ,NUMERO DE VEZES
	dilate = cv2.dilate(erode, np.ones((3,3), np.uint8),iterations = 1)
						#IMAGEM,MATRIZ                 ,NUMERO DE VEZES
	cv2.bitwise_not(dilate)

	return dilate #RETORNA A ULTIMA IMAGEM


def update(*arg):
	#CRIA TRACKBAR

	h0 = cv2.getTrackbarPos('Cor min          ', 'Control')#Faixa de cor
	h1 = cv2.getTrackbarPos('Cor max          ', 'Control')#Faixa de cor
	s0 = cv2.getTrackbarPos('Saturação min    ', 'Control')#Faixa de saturação
	s1 = cv2.getTrackbarPos('Saturação max    ', 'Control')#Faixa de saturação
	v0 = cv2.getTrackbarPos('Luminosidade min ', 'Control')#Faixa de luminosidade
	v1 = cv2.getTrackbarPos('Luminosidade max ', 'Control')#Faixa de luminosidade


	lower = np.array((h0,s0,v0))
	upper = np.array((h1,s1,v1))
	mask = cv2.inRange(hsv, lower, upper) #RESULTADO DA IMAGEM JA COM A APLICACAO DO TRACKBAR


#	cv2.namedWindow('control_mask', 0) #CRIA UMA JANELA
#	cv2.imshow('control_mask', mask) #MOSTRA A MASK(A IMAGEM COM FILTRO HSV)
	global dilate
   	dilate = filter_mask(mask) #USA A FUNCAO FILTER_MASK(1 FUNCAO DECLARADA QUE APLICA O FILTRO) OS FILTROS ESTAO SENDO APLICADOS NA MASK

   	#Mostrar a Imagem original 
	cv2.namedWindow('Original',  cv2.WINDOW_NORMAL)
	cv2.imshow('Original', src)


   	cv2.namedWindow('control_dilate', 0) #CRIA JANELA
	cv2.imshow('control_dilate', dilate) #MOSTRA IMAGEM COM FILTRO DE DILATACAOcv2.bitwise_not(dilate)
	

	cv2.imwrite('id_praga_bin.png',dilate) #salva imagem com filtro hsv e dilatacao

	###INICIO DA CONTAGEM####################
	im2, contours, hierarchy = cv2.findContours(dilate,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
	total_contours = len(contours)
	result=src.copy()


	#cv2.drawContours(result, contours, 1, (0,255,0), 3)

	contours_error = []
	global areatotal
	global TOTAL_2
	areatotal = 0
	contours_ok=[]
	for cont_contours in contours:
		area = cv2.contourArea (cont_contours)
#		print ("area: ", area)
		contours_ok.append(cont_contours)
		areatotal = areatotal + area
#	print ("area total: ", areatotal)


	#Definir certo e errado
	cv2.drawContours(result, contours_ok, -1, (0,255,0), 2)
	cv2.drawContours(result, contours_error, -1, (255,0,0), 2)

	#Ler o que é branco
	l = cv2.imread('id_praga_bin.png')
	MAX_2 = np.array([255, 255, 255], np.uint8)
	MIN_2 = np.array([255, 255, 255], np.uint8)

	dst_2 = cv2.inRange(l, MIN_2, MAX_2)
	TOTAL_2 = cv2.countNonZero(dst_2)

#	print ("TOTAL: ",total_contours)
#	print ("OK_IF: ", len(contours_ok))
#	print ("ERROR_IF: ", len(contours_error))


	cv2.namedWindow('control_result', 0)
	cv2.imshow ('control_result', result)





def main():
	cv2.namedWindow   ('Control', 0)  # CRIA UMA JANELA COM OS TRACKBAR
	cv2.createTrackbar('Cor min          ', 'Control', 0  , 255, update)
	cv2.createTrackbar('Cor max          ', 'Control', 165, 255, update)
	cv2.createTrackbar('Saturação min    ', 'Control', 0  , 255, update)
	cv2.createTrackbar('Saturação max    ', 'Control', 146, 255, update)
	cv2.createTrackbar('Luminosidade min ', 'Control', 113, 255, update)
	cv2.createTrackbar('Luminosidade max ', 'Control', 250, 255, update)
#	trackbar(nome controle, janela, default, max, funcao)


	cv2.imshow('Control',src) #MOSTRA A IMAGEM ORIGINAL ABAIXO NA JANELA DO TRACKBAR
	update() #CHAMA A FUNCAO UPDATE

	global hectare
	global areatotal
	global TOTAL_1
	global TOTAL_2
	global extensao
	while 1:
		ch = cv2.waitKey(200)
		if (ch == 32):
			v = TOTAL_1
			c = TOTAL_2
			print ("Área Afetada: ", c)
			print ("Área total:   ", v)
			print ("Número total de hectares: ", hectare)
			area_afetada = (c*100.0)/v
			print ('Porcentagem da área afetada: %0.3f' %(area_afetada))
			hectare_afetado = (area_afetada * hectare) / 100.0
			print ("Número de Hectares afetado:  ", hectare_afetado, "hectares")
			print (extensao)
		if (ch == 113):
			global dilate
			dilate = cv2.bitwise_not(dilate)
			cv2.namedWindow('control_dilate', 0) #CRIA JANELA
			cv2.imshow('control_dilate', dilate)
		if (ch == 119):
			cv2.namedWindow   ('Control', 0)  # CRIA UMA JANELA COM OS TRACKBAR
			cv2.createTrackbar('Cor min          ', 'Control', 0  , 255, update)
			cv2.createTrackbar('Cor max          ', 'Control', 165, 255, update)
			cv2.createTrackbar('Saturação min    ', 'Control', 0  , 255, update)
			cv2.createTrackbar('Saturação max    ', 'Control', 146, 255, update)
			cv2.createTrackbar('Luminosidade min ', 'Control', 183, 255, update)
			cv2.createTrackbar('Luminosidade max ', 'Control', 250, 255, update)
		if (ch == 120):
			cv2.namedWindow   ('Control', 0)  # CRIA UMA JANELA COM OS TRACKBAR
			cv2.createTrackbar('Cor min          ', 'Control', 0  , 255, update)
			cv2.createTrackbar('Cor max          ', 'Control', 165, 255, update)
			cv2.createTrackbar('Saturação min    ', 'Control', 0  , 255, update)
			cv2.createTrackbar('Saturação max    ', 'Control', 146, 255, update)
			cv2.createTrackbar('Luminosidade min ', 'Control', 90, 255, update)
			cv2.createTrackbar('Luminosidade max ', 'Control', 250, 255, update)
		if (ch == 27):
			break
			cv2.destroyAllWindows()
################################## INICIO DO PROGRAMA########################################
local = ('/home/johan/Downloads')
#local    = raw_input("Insira o local:" + "\n")
imagem_1 = str('0011.jpg')
#imagem_1 = raw_input("Insira o nome da imagem com extensão:" + "\n")
global hectare
hectare = float(166)
#hectare  = float(input("Insira número de Hectare:" + "\n"))
extensao = str('jpg')
#extensao = raw_input("Insira a extensão da imagem:" + "\n")




im = (local + "/" + imagem_1)


if __name__ == '__main__':
	import sys
	try:
		fn = sys.argv[1]
		print("parametro:", fn)
	except:
		fn = (local + "/" + imagem_1) #############lOCAL DA IMAGEM##### #################

		src = cv2.imread(fn)


	#Classificação do que não é a imagem do talhão JPG
	if  "jpg" == extensao:
		global fh
		src = cv2.imread(fn)  ######FUNCAO QUE LER A IMAGEM#########################
		src = cv2.resize(src, None, fx=1, fy=1, interpolation=cv2.INTER_CUBIC) ###########REDIMENSIONAMENTO DA IMAGEM##################
		MAX_1 = np.array([255, 254, 250], np.uint8)
		MIN_1 = np.array([0, 0, 0], np.uint8)

		global TOTAL_1
		dst_1 = cv2.inRange(src, MIN_1, MAX_1)
		TOTAL_1 = cv2.countNonZero(dst_1)
		#print('The number of blue pixels is: ' + str(no_blue))


    #Classificação do que não é a imagem do talhão TIFF
	elif "tiff" == extensao:
		global fh
		src = cv2.imread(fn)  ######FUNCAO QUE LÊ A IMAGEM#########################
		src = cv2.resize(src, None, fx=1, fy=1, interpolation=cv2.INTER_CUBIC) ###########REDIMENSIONAMENTO DA IMAGEM##################
		MAX_1 = np.array([250, 250, 253], np.uint8)
		MIN_1 = np.array([0, 10, 10], np.uint8)

		global TOTAL_1
		dst_1 = cv2.inRange(src, MIN_1, MAX_1)
		TOTAL_1 = cv2.countNonZero(dst_1)
		#print('The number of blue pixels is: ' + str(no_blue))

	#cv2.imshow('Original',src) ##FUNCAO CASO QUEIRA MOSTRAR A IMAGEM
	#print("resized shape:", src.shape)
	hsv = cv2.cvtColor(src, cv2.COLOR_BGR2HSV) #faz a conversao da imagem de rgb para hsv
	cv2.waitKey(0)


	main() #CHAMA A FUNCAO MAIN

